export IMAGE_NAME="rails7"
podman manifest create ${IMAGE_NAME}
podman build --platform linux/amd64,linux/arm64  --manifest ${IMAGE_NAME} .